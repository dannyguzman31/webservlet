package Web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends  HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String fName = request.getParameter("firstname");
        String lname = request.getParameter("lastname");
        String country = request.getParameter("country");
        String email = request.getParameter("email");
        String phone = request.getParameter("phoneNumb");
        String time = request.getParameter("time");

        out.println("<h3>Thank you for your information</h3>");
        out.println("<p>" + fName + " " + lname + ", one of our representatives from " + country + "</p>");
        out.println("<p> will get in touch with you during the " + time + " time and </p>");
        out.println("<p> call you at the following number " + phone + ".</p>");
        out.println("<p> You will also receive a confirmation email at " + email + "</p>");
        out.println("<p> Thank you for your interest! </p>");
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Error thrown!");
    }

}
